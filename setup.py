from setuptools import setup

setup(
    name='oceanides_data_cubes',
    version='0.1',
    description='A Data Cube framework for handling Earth Observation and Climate Change data.',
    author='Ioannis Kavouras',
    author_email='ikavouras@mail.ntua.gr',
    # packages=['my_package'],
    install_requires=[
        'numpy',
        'pillow',
        'xtarfile',
        'pyproj',
        'xarray',
        'rioxarray',
        'rasterio',
        'xmltodict',
        'geopandas',
        'shapely',
        'eoreader',
    ],
)
