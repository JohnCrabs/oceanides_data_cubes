# oceanides_data_cubes

## Description
This project aims to provide a powerful tool for generating 
remote sensing data cubes. The proposed framework is capable 
of data harmonization and homogenization from different satellite
sources in a netCDF file format. This permits the easy distribution
of the data and ensures the compatibility among the various software.

## Dependencies
- cdsapi~=0.6.1
- xtarfile~=0.2.1
- numpy~=1.26.4
- rioxarray~=0.15.0
- rasterio~=1.3.9
- xarray~=2024.1.1
- xmltodict~=0.13.0
- geopandas~=0.14.3
- eoreader~=0.21.0.post0
- pyproj~=3.6.1
- pillow~=10.2.0
- shapely~=2.0.3
- setuptools~=58.1.0

## Installation
After cloning the repository run the requirements.txt to set 
up the virtual environment.

``
pip install -r requirements.txt
``

## Usage
To be added soon...


## License
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Project status
Currently, in the development stage.
