import os
import shutil
import stat
import math
from src.smart_datacubes.odc_framework.eo.eo_products import *
from pyproj import CRS
from PIL import Image
from zipfile import ZipFile

import xtarfile as tarfile
import rioxarray as rxr
import xmltodict
import geopandas as gpd
from shapely.geometry import box
from eoreader.reader import Reader
from eoreader.bands import *

from src.smart_datacubes.odc_framework.ODC_Flags import *


def remove_readonly(func, path, _):
    # Clear the readonly bit and reattempt the removal
    os.chmod(path, stat.S_IWRITE)
    func(path)


class SmartDatacubes:
    def __init__(self):
        self.in_dir_path = None
        self.ds = {
            "dims": (ODC_DIM_KEY_BAND, ODC_DIM_KEY_LATITUDE, ODC_DIM_KEY_LONGITUDE),
            "coords": {},
            "attrs": {},
            "data_vars": {}
        }
        self.crs = None
        self.epsg = "EPSG:4326"
        self.clipping_coordinates = {
            "min_x": 0,
            "min_y": 0,
            "max_x": 0,
            "max_y": 0
        }
        self.pixel_resolution = 1.0
        self.gpd_clip_mask = gpd.GeoDataFrame()
        self.coord_X = []
        self.coord_Y = []
        self.coord_units_X = "nan"
        self.coord_units_Y = "nan"

    # ****************** #
    # * Static Methods * #
    # ****************** #
    @staticmethod
    def _check_for_remote_sensing_file(in_file: str):
        # Get the satellite id (in raw files is the first part, i.e. LC08 for Landsat-8 data)
        satellite_id = in_file.split("_")[0]

        # if id belongs to LANDSAT-8
        if satellite_id in FLAG_LIST_LANDSAT_8:
            return FLAG_LANDSAT_8, True

        # if id belongs to Sentinel-2
        elif satellite_id in FLAG_LIST_SENTINEL_2:
            return FLAG_SENTINEL_2, True

        # Return unknown product for unsupported products or wrong files
        return FLAG_UNKNOWN_PRODUCT, False

    @staticmethod
    def _check_archived_files_and_unarchived_them(in_file: str, dir_path: str):
        abs_file_path = os.path.normpath(os.path.normpath(dir_path) + "/" + in_file)
        # Check if file is in ZIP format (usual format of Sentinel Data)
        if in_file.__contains__(".zip"):
            full_path = os.path.normpath(os.path.normpath(dir_path) + "/" + in_file.split(".zip")[0])
            with ZipFile(abs_file_path) as ZipObject:
                ZipObject.extractall(os.path.normpath(dir_path))
            return full_path, True
        # Check if file is in TAR format (usual format of Landsat Data)
        elif in_file.__contains__(".tar"):
            full_path = os.path.normpath(os.path.normpath(dir_path) + "/" + in_file.split(".tar")[0])
            with tarfile.open(abs_file_path) as TarObject:
                TarObject.extractall(full_path)
                TarObject.close()
            return full_path, True
        # Check if file is in Directory (extracted by the user)
        elif os.path.isdir(abs_file_path):
            return abs_file_path, False
        # Other data format are currently unsupported
        else:
            return None, False

    # LANDSAT-8
    @staticmethod
    def _read_Landsat_Metadata_File_from_txt(in_file_path):
        # Local function for recursing
        def _read_metadata_group(metadataFile, start):
            tmp_metadata = {}  # declare an empty temporary dictionary
            index = start  # set an index parameter equals to start
            # while index is less than the file_size (lines of the txt file)
            while index < metadataFile.__len__():
                # remove all spaces from the line and split on the first "=" character
                line_split = metadataFile[index].strip().replace(" = ", "=").replace(" =", "=").replace("= ", "=")
                line_split = line_split.split('=', 1)

                # If the line split is equal to 2 parts (GROUP, END_GROUP, or PARAMETER)
                if line_split.__len__() == 2:
                    command = line_split[0]  # Get the command/parameter_name
                    value = line_split[1]  # Get the value
                    # command == GROUP
                    if command == 'GROUP':
                        # Recurse for reading the parameter values of the new group
                        tmp_metadata[value], index = _read_metadata_group(metadataFile, index + 1)
                    # command == END_GROUP
                    elif command == 'END_GROUP':
                        # return the dictionary with the parameter values and the current index value
                        return tmp_metadata, index
                    # command is PARAMETER
                    else:
                        # Add the parameter to the dictionary
                        tmp_metadata[command] = str(value.replace('"', ''))
                # If line split is not equal to 2 (error case or END)
                else:
                    # return the dictionary with the parameter values and the current index value
                    return tmp_metadata, index
                index += 1  # increase the index
            # return the dictionary with the parameter values and the current index value (safety return)
            return tmp_metadata, index

        # set metadata to empty dict
        metadata = {}
        # Check if file exists
        if os.path.exists(in_file_path):
            # Read all lines in the txt file
            with open(in_file_path) as __file__:
                metadata_file = __file__.readlines()
                __file__.close()
            # Read metadata
            metadata, _ = _read_metadata_group(metadata_file, 0)
        return metadata  # return the metadata

    @staticmethod
    def _get_Landsat_tiff_file_names(in_dir_path: str, metadata: {} = None):
        def read_tiff_from_path():
            tiff_files = []
            _files_in_dir = os.listdir(in_dir_path)  # List all the files in directory
            for __ind__ in range(1, 12):
                _file_name = [x for x in _files_in_dir if x.__contains__("_B" + str(__ind__))]
                if _file_name.__len__() > 0:
                    tiff_files.append(_file_name[0])
            return tiff_files

        tiff_file_names = []
        # Check if metadata is not in the correct format
        if metadata is None or type(metadata) is not type({}):
            tiff_file_names = read_tiff_from_path()
        # If metadata is in the correct format
        else:
            # Check if metadata has been read correctly (is not broken) - contains the appropriate flags
            if (META_FLAG_LC08_LANDSAT_METADATA_FILE in metadata.keys() and
                    META_FLAG_LC08_PRODUCT_CONTENTS in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE].keys()):
                product_contents = metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_PRODUCT_CONTENTS]
                for __index__ in range(1, 12):
                    if META_FLAG_FILE_NAME_BAND_ + str(__index__) in product_contents.keys():
                        tiff_file_names.append(product_contents[META_FLAG_FILE_NAME_BAND_ + str(__index__)])
            # Check if the broken metadata contains the appropriate information
            elif META_FLAG_LC08_PRODUCT_CONTENTS in metadata.keys():
                product_contents = metadata[META_FLAG_LC08_PRODUCT_CONTENTS]
                for __index__ in range(1, 12):
                    if META_FLAG_FILE_NAME_BAND_ + str(__index__) in product_contents.keys():
                        tiff_file_names.append(product_contents[META_FLAG_FILE_NAME_BAND_ + str(__index__)])
            # Check if the broken metadata contains the appropriate information
            else:
                tiff_file_names = read_tiff_from_path()

            if tiff_file_names is []:
                tiff_file_names = read_tiff_from_path()

        return tiff_file_names

    @staticmethod
    def _get_Landsat_band_resolution(metadata):
        def get_default_landsat_resolutions():
            tmp_b_res_dict = {}
            # Parse all the grid cell size types
            for __b__ in DICT_FLAG_LANDSAT_GRID_CELL_SIZE_TYPE:
                # if the grid is the panchromatic
                if DICT_FLAG_LANDSAT_GRID_CELL_SIZE_TYPE[__b__] == META_FLAG_GRID_CELL_SIZE_PANCHROMATIC:
                    # set the resolution to 15.00m
                    tmp_b_res_dict[DICT_FLAG_LANDSAT_DATASET[__b__]] = float(15.00)
                else:
                    # else set the resolution to 15.00m
                    tmp_b_res_dict[DICT_FLAG_LANDSAT_DATASET[__b__]] = float(30.00)
            return tmp_b_res_dict

        # Declare some local variables for code readability
        landsat_metadata_file = META_FLAG_LC08_LANDSAT_METADATA_FILE
        projection_attrs = META_FLAG_LC08_PROJECTION_ATTRIBUTES
        grid_cell_size_type = DICT_FLAG_LANDSAT_GRID_CELL_SIZE_TYPE
        landsat_dataset = DICT_FLAG_LANDSAT_DATASET

        # Declare the band resolution dictionary
        band_resolution_dict = {}

        # Check if metadata exists
        if metadata is not None:
            # Check if is the correct format
            if (landsat_metadata_file in metadata.keys() and
                    projection_attrs in metadata[landsat_metadata_file].keys()):

                # Parse all bands in grid_cess_size_types
                for __band__ in grid_cell_size_type.keys():
                    # if grid exists in the metadata file
                    if grid_cell_size_type[__band__] in metadata[landsat_metadata_file][projection_attrs].keys():
                        # add the cell_size
                        cell_size = metadata[landsat_metadata_file][projection_attrs][grid_cell_size_type[__band__]]
                        band_resolution_dict[landsat_dataset[__band__]] = float(cell_size)
                    else:
                        # Add the default cell size for this band
                        if grid_cell_size_type[__band__] == META_FLAG_GRID_CELL_SIZE_PANCHROMATIC:
                            band_resolution_dict[landsat_dataset[__band__]] = float(15.00)
                        else:
                            band_resolution_dict[landsat_dataset[__band__]] = float(30.00)
            else:
                # If there is some kind of error in metadata reading, use the default Landsat-8 grid_cess_size
                band_resolution_dict = get_default_landsat_resolutions()
        else:
            # If metadata does not exists, use the default Landsat-8 grid_cess_size
            band_resolution_dict = get_default_landsat_resolutions()
        return band_resolution_dict

    # TODO Clean and Optimize this function
    @staticmethod
    def _get_radiometric_correction_factors(metadata):
        radiometric_correction = {}
        reflectance_correction = {}
        sun_elevation = 0
        success = True

        if META_FLAG_LC08_LANDSAT_METADATA_FILE in metadata.keys():
            if META_FLAG_LC08_IMAGE_ATTRIBUTES in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE].keys() and \
                    META_FLAG_SUN_ELEVATION in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                META_FLAG_LC08_IMAGE_ATTRIBUTES].keys():
                sun_elevation = math.radians(float(
                    metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_IMAGE_ATTRIBUTES][
                        META_FLAG_SUN_ELEVATION]))
            else:
                success = False

            if META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE].keys():
                for __key__ in DICT_FLAG_LANDSAT_DATASET.keys():
                    if __key__ in DICT_FLAG_RADIANCE_MULT_BAND.keys() and __key__ in DICT_FLAG_RADIANCE_ADD_BAND.keys() and \
                            DICT_FLAG_RADIANCE_MULT_BAND[__key__] in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                        META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING].keys() and \
                            DICT_FLAG_RADIANCE_ADD_BAND[__key__] in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                        META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING].keys():
                        radiometric_correction[DICT_FLAG_LANDSAT_DATASET[__key__]] = {
                            'mul': float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                                             META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING][
                                             DICT_FLAG_RADIANCE_MULT_BAND[__key__]]),
                            'add': float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                                             META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING][
                                             DICT_FLAG_RADIANCE_ADD_BAND[__key__]])
                        }
                    if __key__ in DICT_FLAG_REFLECTANCE_MULT_BAND.keys() and __key__ in DICT_FLAG_REFLECTANCE_ADD_BAND.keys() and \
                            DICT_FLAG_REFLECTANCE_MULT_BAND[__key__] in \
                            metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                                META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING].keys() and \
                            DICT_FLAG_REFLECTANCE_ADD_BAND[__key__] in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                        META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING].keys():
                        reflectance_correction[DICT_FLAG_LANDSAT_DATASET[__key__]] = {
                            'mul': float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                                             META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING][
                                             DICT_FLAG_REFLECTANCE_MULT_BAND[__key__]]),
                            'add': float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
                                             META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING][
                                             DICT_FLAG_REFLECTANCE_ADD_BAND[__key__]])
                        }
        else:
            success = False

        return radiometric_correction, reflectance_correction, sun_elevation, success

    # TODO Clean and Optimize this function
    @staticmethod
    def _get_thermal_constants(metadata):
        k1_b10 = 0
        k2_b10 = 0
        k1_b11 = 0
        k2_b11 = 0
        success = True
        if META_FLAG_LC08_LANDSAT_METADATA_FILE in metadata.keys() and \
                META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE].keys() and \
                META_FLAG_K1_CONSTANT_BAND_10 in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
            META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS].keys() and \
                META_FLAG_K2_CONSTANT_BAND_10 in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
            META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS].keys() and \
                META_FLAG_K1_CONSTANT_BAND_11 in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
            META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS].keys() and \
                META_FLAG_K2_CONSTANT_BAND_11 in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][
            META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS].keys():
            k1_b10 = float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS][
                               META_FLAG_K1_CONSTANT_BAND_10])
            k2_b10 = float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS][
                               META_FLAG_K2_CONSTANT_BAND_10])
            k1_b11 = float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS][
                               META_FLAG_K1_CONSTANT_BAND_11])
            k2_b11 = float(metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS][
                               META_FLAG_K2_CONSTANT_BAND_11])
        else:
            success = False
        return k1_b10, k2_b10, k1_b11, k2_b11, success

    @staticmethod
    def normalize_nd_array(arr: np.ndarray):
        return (arr - np.min(arr)) / (np.max(arr) - np.min(arr))

    @staticmethod
    def _reproject_and_clip(in_xarray, to_epsg, clip_geometry, clip_crs):
        o_xarray = in_xarray.rio.reproject(to_epsg)
        o_xarray = o_xarray.rio.clip(clip_geometry, clip_crs)
        o_xarray = o_xarray.astype(dtype=np.float32)
        in_xarray.close()
        return o_xarray

    # ******************************* #
    # * External Callable Functions * #
    # ******************************* #
    def Generate(self, in_dir_path: str, in_clipping_shp_path: str, spatial_resolution_meters: float = 10.0):
        """
        Generates the Data Cubes implementation
        :param in_dir_path:
        :param in_clipping_shp_path:
        :param spatial_resolution_meters:
        :return:
        """
        # Print info messages
        print(f"Input Folder Path: {in_dir_path}")
        print(f"Input Clipping Path: {in_clipping_shp_path}")

        # Check if the folder and clipping paths are existing
        if os.path.exists(in_clipping_shp_path):
            # Calculating the clipping parameters and check if successfully
            if self._calculate_clipping_parameters(in_clipping_shp_path=in_clipping_shp_path,
                                                   spatial_resolution_meters=spatial_resolution_meters):
                # Generating the datacubes
                print("Generate Datacubes")
                self.in_dir_path = in_dir_path  # Store the in_dir_path path
                files_in_path = os.listdir(in_dir_path)  # list all files in the path
                # For each file
                for __file__ in files_in_path:
                    # check the satellite source from the file name
                    satellite_source, is_rs_data = self._check_for_remote_sensing_file(__file__)
                    if is_rs_data:
                        print(f"File named: {__file__} is of type {satellite_source}")
                        # Check if is archived and extract it
                        full_path, is_archived = self._check_archived_files_and_unarchived_them(__file__, in_dir_path)

                        # If source is LANDSAT-8
                        if satellite_source == FLAG_LANDSAT_8 and full_path is not None:
                            # Follow the analysis for Landsat-8 Data and add them to cube
                            self._add_Landsat_8_data_to_cube(full_path)

                        # If source is SENTINEL-2
                        elif satellite_source == FLAG_SENTINEL_2 and full_path is not None:
                            # Follow the analysis for Sentinel-2 Data and add them to cube
                            self._add_Sentinel_2_data_to_cube(full_path)
                        if is_archived:  # If file is archived, then delete the file
                            shutil.rmtree(full_path, ignore_errors=True, onerror=None)
                    else:
                        print(f"File named: {__file__} is of type {satellite_source}")
        else:
            print(f"Path doesn't exist: {in_dir_path}")

    def save_as_NetCDF(self, file_name: str = "SmartCube.nc", out_dir_path=None):
        final_output_path = out_dir_path
        if not file_name.__contains__(".nc"):
            file_name += ".nc"
        if final_output_path is None:
            if self.in_dir_path is None:
                final_output_path = file_name
            else:
                final_output_path = os.path.normpath(os.path.normpath(self.in_dir_path) + "/" + file_name)
        else:
            final_output_path = os.path.normpath(os.path.normpath(final_output_path) + "/" + file_name)

        xds = xr.Dataset.from_dict(self.ds)
        xds.to_netcdf(final_output_path)

    # **************************** #
    # * Class Callable functions * #
    # **************************** #

    def _calculate_clipping_parameters(self, in_clipping_shp_path: str, spatial_resolution_meters: float):
        # Check if the given path exists
        if os.path.exists(in_clipping_shp_path):
            shp_geometry = gpd.read_file(in_clipping_shp_path)  # Read the geometry

            # Get the crs of the geometry and store it
            crs_id = shp_geometry['geometry'].crs.to_epsg()
            self.epsg = f"EPSG:{crs_id}"
            self.crs = CRS.from_epsg(crs_id)
            self.coord_units_X = self.crs.axis_info[0].unit_name
            self.coord_units_Y = self.crs.axis_info[1].unit_name

            # Calculate the geometry's bounds and the coordinates. Stores them as well.
            geom_bounds = gpd.GeoDataFrame(geometry=gpd.GeoSeries(shp_geometry['geometry']))['geometry'].bounds
            step = float(spatial_resolution_meters)
            x_min, x_max = float(geom_bounds['minx'].iloc[0]), float(geom_bounds['maxx'].iloc[0])
            y_min, y_max = float(geom_bounds['miny'].iloc[0]), float(geom_bounds['maxy'].iloc[0])

            self.pixel_resolution = step
            self.coord_X = np.arange(x_min, x_max, step)
            self.coord_Y = np.arange(y_max, y_min, -step)

            # Create a GeoDataFrame for clipping rioxarray products
            self.gpd_clip_mask = gpd.GeoDataFrame(
                geometry=[
                    box(self.coord_X.min(), self.coord_Y.min(),
                        self.coord_X.max(), self.coord_Y.max())],
                crs=self.crs
            )

            attr_value = {}
            for __index__ in range(FLAG_LIST_ODC_BANDS.__len__()):
                attr_value[f"Band {str(__index__ + 1).rjust(2, '0')}"] = FLAG_LIST_ODC_BANDS[__index__]
            self.ds["coords"][ODC_DIM_KEY_BAND] = {"dims": tuple([ODC_DIM_KEY_BAND]),
                                                   "data": np.array(FLAG_LIST_ODC_BANDS), "attrs": attr_value}
            self.ds["coords"][ODC_DIM_KEY_LATITUDE] = {"dims": tuple([ODC_DIM_KEY_LATITUDE]),
                                                       "data": np.array(self.coord_Y),
                                                       "attrs": {"units": self.coord_units_Y}}
            self.ds["coords"][ODC_DIM_KEY_LONGITUDE] = {"dims": tuple([ODC_DIM_KEY_LONGITUDE]),
                                                        "data": np.array(self.coord_X),
                                                        "attrs": {"units": self.coord_units_X}}

        else:
            print(f"Error::_calculate_clipping_parameters(...)::Path <{in_clipping_shp_path}> does not exist!")
            return False
        return True

    @staticmethod
    def _pansharpening(data_dict: {}, pan_band: str):
        pan_dataset = {}
        for __key__ in data_dict.keys():
            curr_img = data_dict[__key__].data
            curr_img = Image.fromarray(curr_img)
            curr_img = curr_img.resize((data_dict[pan_band].shape[1], data_dict[pan_band].shape[0]))
            pan_dataset[__key__] = np.array(curr_img)
        return pan_dataset

    def _generate_data_vars(self, data_dict: {}):
        # calculate the final image resolution
        final_image_resolution = (self.coord_X.__len__(), self.coord_Y.__len__())

        # declare an empty data_vars dictionary to store the results
        data_vars = {}
        for __key__ in data_dict.keys():
            curr_img = Image.fromarray(data_dict[__key__])
            curr_img = curr_img.resize(final_image_resolution)
            curr_img = np.array(curr_img)
            data_vars[__key__] = curr_img

        return data_vars

    def _add_Landsat_8_data_to_cube(self, in_dir_path: str):
        files_in_dir = os.listdir(in_dir_path)  # List all the files in directory
        # Check if at least one metadata file exists in directory
        metadata_files = [x for x in files_in_dir if x.__contains__("MTL")]
        metadata = None
        if metadata_files.__len__() > 0:  # try to read landsat metadata
            metadata = self._read_Landsat_8_Metadata(in_dir_path=in_dir_path, metadata_files=metadata_files)

        # get landsat geotiff file names
        tiff_file_names = self._get_Landsat_tiff_file_names(in_dir_path=in_dir_path, metadata=metadata)

        if tiff_file_names.__len__() > 0:  # error check if list is not empty
            # read landsat raw_dataset and clip
            raw_dataset = self._read_Landsat_8_Dataset(in_dir_path=in_dir_path,
                                                       tiff_file_names=tiff_file_names)

            # add raw dataset to cube
            if raw_dataset.__len__() > 0:
                self._add_Landsat_8_dataset_to_cube(raw_dataset=raw_dataset,
                                                    metadata=metadata,
                                                    data_var_name=os.path.basename(in_dir_path))

    def _read_Landsat_8_Metadata(self, in_dir_path: str, metadata_files: list):
        # Check if the xml file exists in the directory
        xml_file = [x for x in metadata_files if x.__contains__(".xml")]
        if xml_file.__len__() == 0:  # if xml file does not exist
            # Then get the txt file and read it
            txt_file = [x for x in metadata_files if x.__contains__(".txt")][0]
            metadata = self._read_Landsat_Metadata_File_from_txt(
                in_file_path=os.path.normpath(os.path.normpath(in_dir_path) + "/" + txt_file)
            )
        else:  # Read the metadata from the xml file
            xml_file = xml_file[0]
            with open(os.path.normpath(os.path.normpath(in_dir_path) + "/" + xml_file)) as xml_file:
                metadata = xmltodict.parse(xml_file.read())
                xml_file.close()
        return metadata

    def _read_Landsat_8_Dataset(self, in_dir_path: str, tiff_file_names: []):
        tmp_dataset = {}
        for __key__ in DICT_FLAG_LANDSAT_DATASET.keys():
            for __file_name__ in tiff_file_names:
                if __file_name__.__contains__(__key__):
                    with rxr.open_rasterio(
                            os.path.normpath(os.path.normpath(in_dir_path) + '/' + __file_name__),
                            cache=False
                    ) as raster:
                        raster_data = self._reproject_and_clip(
                            in_xarray=raster,
                            to_epsg=self.epsg,
                            clip_geometry=self.gpd_clip_mask["geometry"],
                            clip_crs=self.crs
                        )
                        tmp_dataset[DICT_FLAG_LANDSAT_DATASET[__key__]] = raster_data.data[0]
                        raster_data.close()
                        raster.close()
                    break
        return tmp_dataset

    # TODO Clean and Optimize this function
    def _add_Landsat_8_dataset_to_cube(self, raw_dataset, metadata, data_var_name):
        # Calculate the new pixel resolution ratio and Pseudo-Spatial Analysis
        final_image_resolution = (self.coord_Y.__len__(), self.coord_X.__len__())

        data_vars = self._generate_data_vars(data_dict=raw_dataset)

        # Radiometric correction
        (radiometric_correction, reflectance_correction,
         sun_elevation, success_toa) = self._get_radiometric_correction_factors(metadata)

        top_of_atmosphere_radiance = {}
        if success_toa:
            for __key__ in data_vars.keys():
                if __key__ in radiometric_correction.keys():
                    top_of_atmosphere_radiance[__key__] = data_vars[__key__] * radiometric_correction[__key__]['mul'] + \
                                                          radiometric_correction[__key__]['add']

                if __key__ in reflectance_correction.keys():
                    data_vars[__key__] = (data_vars[__key__] * reflectance_correction[__key__]['mul'] +
                                          reflectance_correction[__key__]['add']) / math.sin(sun_elevation)

        # Get thermal constants for calculating Land surface temperature
        k1_b10, k2_b10, k1_b11, k2_b11, success_thermal = self._get_thermal_constants(metadata)

        # Calculate Extra Products
        data_vars[ODC_BAND_B17_NDVI] = calc_ndvi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED])
        data_vars[ODC_BAND_B18_NDWI] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B5_NIR])
        data_vars[ODC_BAND_B23_LSWI_1] = calc_lswi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B24_LSWI_2] = calc_lswi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B25_ARVI] = calc_arvi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED],
                                                 data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B26_MSAVI2] = calc_msavi2(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED])
        data_vars[ODC_BAND_B27_MTVI2] = calc_mtvi2(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED],
                                                   data_vars[ODC_BAND_B3_GREEN])
        data_vars[ODC_BAND_B28_VARI] = calc_vari(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B29_TGI] = calc_tgi(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                               data_vars[ODC_BAND_B2_BLUE])
        if success_toa and success_thermal:
            data_vars[ODC_BAND_B30_LST_1] = calc_lst(k1_b10, k2_b10, top_of_atmosphere_radiance[ODC_BAND_B15_TIRS_1])
            data_vars[ODC_BAND_B31_LST_2] = calc_lst(k1_b11, k2_b11, top_of_atmosphere_radiance[ODC_BAND_B16_TIRS_2])
            data_vars[ODC_BAND_B32_LST_CELSIUS] = ((data_vars[ODC_BAND_B30_LST_1] + data_vars[
                ODC_BAND_B31_LST_2]) / 2) - 273.15
            data_vars[ODC_BAND_B33_LST_FAHRENHEIT] = data_vars[ODC_BAND_B32_LST_CELSIUS] * 1.8 + 32.0
        data_vars[ODC_BAND_B34_VCI] = calc_vci(data_vars[ODC_BAND_B17_NDVI])
        data_vars[ODC_BAND_B35_MNDWI_1] = calc_mndwi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B36_MNDWI_2] = calc_mndwi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B37_WRI_1] = calc_wri(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B38_WRI_2] = calc_wri(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B39_NDTI] = calc_ndti(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN])
        data_vars[ODC_BAND_B40_AWEI] = calc_awei(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B5_NIR],
                                                 data_vars[ODC_BAND_B6_SWIR_1], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B41_OSI] = calc_osi(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                               data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B42_NBR_1] = calc_nbr(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B43_NBR_2] = calc_nbr(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])

        final_data_arr = []
        for __key__ in FLAG_LIST_ODC_BANDS:
            if __key__ not in data_vars.keys():
                final_data_arr.append(np.zeros(final_image_resolution))
            else:
                final_data_arr.append(data_vars[__key__])
        final_data_arr = np.array(final_data_arr)

        # Add data to the cube
        long_name = []
        for __index__ in range(FLAG_LIST_ODC_BANDS.__len__()):
            long_name.append(f"B{__index__ + 1}, {FLAG_LIST_ODC_BANDS[__index__]}")
        long_name = tuple(long_name)

        self.ds["data_vars"][data_var_name] = {
            "dims": (ODC_DIM_KEY_BAND, ODC_DIM_KEY_LATITUDE, ODC_DIM_KEY_LONGITUDE),
            "data": final_data_arr,
            "attrs": {
                'crs': self.crs.to_string(),
                'long_name': long_name
            }
        }

        return True

    # TODO Add Sentinel-2 functionality
    def _add_Sentinel_2_data_to_cube(self, in_dir_path: str):
        data_var_name = os.path.basename(in_dir_path)
        data_dict = self._read_Sentinel_2_Dataset(in_dir_path=in_dir_path)
        # Calculate the new pixel resolution ratio and Pseudo-Spatial Analysis
        final_image_resolution = (self.coord_Y.__len__(), self.coord_X.__len__())

        data_vars = self._generate_data_vars(data_dict)

        # Calculate Extra Products
        data_vars[ODC_BAND_B17_NDVI] = calc_ndvi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED])
        data_vars[ODC_BAND_B18_NDWI] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B5_NIR])
        data_vars[ODC_BAND_B19_NDVI_RED_EDGE_1] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B9_VRE_1])
        data_vars[ODC_BAND_B20_NDVI_RED_EDGE_2] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B10_VRE_2])
        data_vars[ODC_BAND_B21_NDVI_RED_EDGE_3] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B11_VRE_3])
        data_vars[ODC_BAND_B22_NDVI_NARROW_NIR] = calc_ndvi(data_vars[ODC_BAND_B3_GREEN],
                                                            data_vars[ODC_BAND_B12_NARROW_NIR])
        data_vars[ODC_BAND_B23_LSWI_1] = calc_lswi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B24_LSWI_2] = calc_lswi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B25_ARVI] = calc_arvi(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED],
                                                 data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B26_MSAVI2] = calc_msavi2(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED])
        data_vars[ODC_BAND_B27_MTVI2] = calc_mtvi2(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B4_RED],
                                                   data_vars[ODC_BAND_B3_GREEN])
        data_vars[ODC_BAND_B28_VARI] = calc_vari(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B29_TGI] = calc_tgi(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                               data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B34_VCI] = calc_vci(data_vars[ODC_BAND_B17_NDVI])
        data_vars[ODC_BAND_B35_MNDWI_1] = calc_mndwi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B36_MNDWI_2] = calc_mndwi(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B37_WRI_1] = calc_wri(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B38_WRI_2] = calc_wri(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                                 data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B39_NDTI] = calc_ndti(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN])
        data_vars[ODC_BAND_B40_AWEI] = calc_awei(data_vars[ODC_BAND_B3_GREEN], data_vars[ODC_BAND_B5_NIR],
                                                 data_vars[ODC_BAND_B6_SWIR_1], data_vars[ODC_BAND_B7_SWIR_2])
        data_vars[ODC_BAND_B41_OSI] = calc_osi(data_vars[ODC_BAND_B4_RED], data_vars[ODC_BAND_B3_GREEN],
                                               data_vars[ODC_BAND_B2_BLUE])
        data_vars[ODC_BAND_B42_NBR_1] = calc_nbr(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B6_SWIR_1])
        data_vars[ODC_BAND_B43_NBR_2] = calc_nbr(data_vars[ODC_BAND_B5_NIR], data_vars[ODC_BAND_B7_SWIR_2])

        final_data_arr = []
        for __key__ in FLAG_LIST_ODC_BANDS:
            if __key__ not in data_vars.keys():
                final_data_arr.append(np.zeros(final_image_resolution))
            else:
                final_data_arr.append(data_vars[__key__])
        final_data_arr = np.array(final_data_arr)

        # Add data to the cube
        long_name = []
        for __index__ in range(FLAG_LIST_ODC_BANDS.__len__()):
            long_name.append(f"B{__index__ + 1}, {FLAG_LIST_ODC_BANDS[__index__]}")
        long_name = tuple(long_name)

        self.ds["data_vars"][data_var_name] = {
            "dims": (ODC_DIM_KEY_BAND, ODC_DIM_KEY_LATITUDE, ODC_DIM_KEY_LONGITUDE),
            "data": final_data_arr,
            "attrs": {
                'crs': self.crs.to_string(),
                'long_name': long_name
            }
        }

        return True

    def _read_Sentinel_2_Dataset(self, in_dir_path: str):
        data_dict = {}
        if os.path.exists(in_dir_path):
            reader = Reader()
            s2_product = reader.open(in_dir_path, remove_tmp=True)

            band_dict = {
                CA: SENTINEL_BAND_B1_COASTAL_AEROSOL,
                BLUE: SENTINEL_BAND_B2_BLUE,
                GREEN: SENTINEL_BAND_B3_GREEN,
                RED: SENTINEL_BAND_B4_RED,
                VRE_1: SENTINEL_BAND_B5_VRE_1,
                VRE_2: SENTINEL_BAND_B6_VRE_2,
                VRE_3: SENTINEL_BAND_B7_VRE_3,
                NIR: SENTINEL_BAND_B8_NIR,
                NARROW_NIR: SENTINEL_BAND_B8A_NARROW_NIR,
                WV: SENTINEL_BAND_B9_WATER_VAPOUR,
                SWIR_CIRRUS: SENTINEL_BAND_B10_CIRRUS,
                SWIR_1: SENTINEL_BAND_B11_SWIR_1,
                SWIR_2: SENTINEL_BAND_B12_SWIR_2
            }

            for __band__ in band_dict.keys():
                if __band__ in s2_product.get_existing_bands():
                    try:
                        with s2_product.load(__band__) as raster:
                            raster_data = self._reproject_and_clip(
                                in_xarray=raster.data_vars[__band__],
                                to_epsg=self.epsg,
                                clip_geometry=self.gpd_clip_mask["geometry"],
                                clip_crs=self.crs
                            )
                            data_dict[band_dict[__band__]] = np.array(raster_data.data[0], dtype=np.float32)
                            raster_data.close()
                            raster.close()
                    except:
                        data_dict[__band__.value] = np.zeros((self.coord_Y.__len__(), self.coord_X.__len__()))
                else:
                    data_dict[__band__.value] = np.zeros((self.coord_Y.__len__(), self.coord_X.__len__()))
            s2_product.clean_tmp()
            s2_product.clear()
        return data_dict


# ****************** #
# * MAIN EXECUTION * #
# ****************** #
if __name__ == "__main__":
    in_path_dir = "../../data/"
    in_clipping_mask_dir = ""

    smartDC = SmartDatacubes()
    smartDC.Generate(
        in_dir_path=in_path_dir,
        in_clipping_shp_path=in_clipping_mask_dir,
        spatial_resolution_meters=10
    )
    smartDC.save_as_NetCDF()
