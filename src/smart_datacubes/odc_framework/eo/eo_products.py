import numpy as np
import geopandas as gpd

from PIL import Image
from pyproj import CRS
from pathlib import Path

from shapely.geometry import box
from rasterio.enums import Resampling

from src.smart_datacubes.odc_framework.ODC_Flags import *
from src.smart_datacubes.odc_framework.odc_framework import FrameworkODC


# ****************************** #
# ***** MAIN EO DATA CLASS ***** #
# ****************************** #
class EarthObservationData(FrameworkODC):
    def __init__(self):
        super().__init__()
        self._data: dict = {}
        self._metadata: dict = {}

    # ***************************** #
    # ***** GETTERS / SETTERS ***** #
    # ***************************** #
    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_data: dict):
        if type(new_data) is dict:
            self._data = new_data

    @property
    def metadata(self):
        return self._metadata

    @metadata.setter
    def metadata(self, new_metadata: dict):
        if type(new_metadata) is dict:
            self._metadata = new_metadata

    # ***************************** #
    # ***** PRIVATE FUNCTIONS ***** #
    # ***************************** #
    def _clip_by_shp(self, shp_path: str or Path):
        fp = Path(shp_path)
        # Check if the given path exists
        if not fp.exists():
            return
        shp_geom = gpd.read_file(shp_path)  # Read the geometry
        geom_epsg_id = shp_geom['geometry'].crs.to_epsg()
        if geom_epsg_id != self.epsg_id:
            shp_geom = shp_geom.to_crs(epsg=self.epsg_id)
        geom_bounds = gpd.GeoDataFrame(geometry=gpd.GeoSeries(shp_geom['geometry']))['geometry'].bounds
        x_min, x_max = float(geom_bounds['minx'].iloc[0]), float(geom_bounds['maxx'].iloc[0])
        y_min, y_max = float(geom_bounds['miny'].iloc[0]), float(geom_bounds['maxy'].iloc[0])

        clip_box = gpd.GeoDataFrame(
            geometry=[
                box(x_min, y_min,
                    x_max, y_max)],
            crs=CRS.from_epsg(self.epsg_id)
        )

        self._clip_by_geometry(clip_box=clip_box)

    def _clip_by_geometry(self, clip_box: gpd.GeoDataFrame):
        clip_data = {}
        if self.data.__len__() > 0:
            for __key__ in self.data.keys():
                print(f"Info:Clip band <{__key__}>")
                clip_data[__key__] = self.data[__key__].rio.clip(clip_box['geometry'], clip_box.crs).fillna(0)
            self.data = clip_data

    # ******************************** #
    # ***** OVERLOADED FUNCTIONS ***** #
    # ******************************** #
    def read(self, path: str):
        super().read(path)

    def reproject(self, epsg_id: str or int or None):
        reproject_data = {}
        if self.data.__len__() > 0 and epsg_id is not None:
            self.epsg_id = int(epsg_id)
            self.epsg = f'EPSG:{self.epsg_id}'
            self.crs = CRS.from_string(self.epsg)
            for __key__ in self.data.keys():
                print(f"Info:Reproject band <{__key__}>")
                reproject_data[__key__] = self.data[__key__].rio.reproject(self.crs)
            self.data = reproject_data

    def clip(self, bounds: str or Path or gpd.GeoDataFrame):
        if type(bounds) in [str, Path]:
            self._clip_by_shp(bounds)
        elif type(bounds) is gpd.GeoDataFrame:
            self._clip_by_geometry(bounds)

    def resample(self, spatial_resolution: float):
        super().resample(spatial_resolution)
        resample_data = {}
        if self.data.__len__() > 0:
            for __key__ in self.data.keys():
                raster = self.data[__key__]
                res_x = (float(raster.x.max()) - float(raster.x.min())) / raster.x.__len__()
                res_y = (float(raster.y.max()) - float(raster.y.min())) / raster.y.__len__()

                resample_factor_x = res_x / spatial_resolution
                resample_factor_y = res_y / spatial_resolution

                # Calculate new height and width using upscale_factor
                new_width = int(raster.rio.width * resample_factor_x)
                new_height = int(raster.rio.height * resample_factor_y)

                print(f"Info:Resample band <{__key__}> to new size ({new_height}, {new_width})")
                # Resample raster
                resample_data[__key__] = raster.rio.reproject(raster.rio.crs,
                                                              shape=(new_height, new_width),
                                                              resampling=Resampling.bilinear)
            self.data = resample_data

    def generate_product(self):
        super().generate_product()
        print(f"Info:Generate product <{self.p_path.name}>")
        self.product = {
            "dims": (ODC_DIM_KEY_BAND, ODC_DIM_KEY_LATITUDE, ODC_DIM_KEY_LONGITUDE),
            "coords": {},
            "attrs": {},
            "data_vars": {
                self.p_path.name: {
                    "dims": (ODC_DIM_KEY_BAND, ODC_DIM_KEY_LATITUDE, ODC_DIM_KEY_LONGITUDE),
                    "data": [],
                    "attrs": {
                        'crs': CRS.from_epsg(self.epsg.split(':')[1]).to_string()
                    }
                }
            },
            "name": self.p_path.name
        }

    def exec_workflow(self, path: str or Path, epsg_id: str or int,
                      bounds: str or Path or gpd.GeoDataFrame, spatial_resolution: float,
                      b_clean_data: bool = True):
        super().exec_workflow(path, epsg_id, bounds, spatial_resolution, b_clean_data)

    def export_as_NetCDF(self, dir_path: str or Path or None = None):
        super().export_as_NetCDF(dir_path)
    
    # ******************************* #
    # ***** DEBUGGING FUNCTIONS ***** #
    # ******************************* #
    def showGray(self, band: str):
        if band in self._data.keys():
            gray = np.array(self.data[band].data / self.data[band].data.max() * 255.0)
            img = Image.fromarray(gray)
            img.show()

    def showRGB(self, bandRed: str, bandGreen: str, bandBlue: str):
        if (bandRed in self._data.keys()
                and bandGreen in self.data.keys() and bandBlue in self.data.keys()):
            red = np.array(self.data[bandRed].data / self.data[bandRed].data.max() * 255.0)
            green = np.array(self.data[bandGreen].data / self.data[bandGreen].data.max() * 255.0)
            blue = np.array(self.data[bandBlue].data / self.data[bandBlue].data.max() * 255.0)
            rgb = np.array([red.T, green.T, blue.T])
            rgb = np.array(rgb.T, dtype=np.uint8)

            img = Image.fromarray(rgb, 'RGB')
            img.show()


# ********************************* #
# ***** MAIN EO DATA PRODUCTS ***** #
# ********************************* #
def calc_ndvi(nir: np.ndarray, red: np.ndarray):
    return (nir - red) / (nir + red)


def calc_ndwi(nir: np.ndarray, green: np.ndarray):
    return (green - nir) / (green + nir)


def calc_lswi(nir: np.ndarray, swir: np.ndarray):
    return (nir - swir) / (nir + swir)


def calc_arvi(nir: np.ndarray, red: np.ndarray, blue: np.ndarray):
    return (nir - 2 * red + blue) / (nir + 2 * red + blue)


def calc_msavi2(nir: np.ndarray, red: np.ndarray):
    return (2 * nir + 1 - np.sqrt(np.square(2 * nir + 1)) - 8 * (nir - red)) / 2


def calc_mtvi2(nir: np.ndarray, red: np.ndarray, green: np.ndarray):
    return (1.5 * (1.2 * (nir - green) - 2.5 * (red - green))) / (
                np.sqrt(np.square(2 * nir + 1) - (6 * nir - 5 * np.sqrt(red))) - 0.5)


def calc_vari(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (green - red) / (green + red - blue)


def calc_tgi(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (120.0 * (red - blue) - 190.0 * (red - green)) / 2


def calc_lst(k1: float, k2: float, toa_radiance: np.ndarray):
    return k2 / np.log((k1 / toa_radiance) + 1)


def calc_vci(ndvi: np.ndarray):
    return (ndvi - ndvi.min()) / (ndvi.max() - ndvi.min())


def calc_mndwi(green: np.ndarray, swir: np.ndarray):
    return (green - swir) / (green + swir)


def calc_wri(red: np.ndarray, green: np.ndarray, nir: np.ndarray, swir: np.ndarray):
    return (green + red) / (nir + swir)


def calc_ndti(red: np.ndarray, green: np.ndarray):
    return (red - green) / (red + green)


def calc_awei(green: np.ndarray, nir: np.ndarray, swir1: np.ndarray, swir2: np.ndarray):
    return 4 * (green - swir2 - 0.25 * nir + 2.75 * swir1)


def calc_osi(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (red + green) / blue


def calc_nbr(nir: np.ndarray, swir: np.ndarray):
    return (nir - swir) / (nir + swir)


if __name__ == '__main__':
    print("Hello world from EO Products!")
