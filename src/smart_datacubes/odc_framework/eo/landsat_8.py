import os
import xmltodict
import rioxarray as rxr
from eo_products import *


class Landsat8(EarthObservationData):
    def __init__(self):
        super().__init__()

    # ************************** #
    # ***** STATIC METHODS ***** #
    # ************************** #
    @staticmethod
    def _find_TIFF_FileNames(in_dir_path: str, metadata: {} = None):
        def read_tiff_from_path():
            tiff_files = []
            _files_in_dir = os.listdir(in_dir_path)  # List all the files in directory
            for __ind__ in range(1, 12):
                _file_name = [x for x in _files_in_dir if x.__contains__("_B" + str(__ind__))]
                if _file_name.__len__() > 0:
                    tiff_files.append(_file_name[0])
            return tiff_files

        tiff_file_names = []
        # Check if metadata is not in the correct format
        if metadata is None or type(metadata) is not type({}):
            tiff_file_names = read_tiff_from_path()
        # If metadata is in the correct format
        else:
            # Check if metadata has been read correctly (is not broken) - contains the appropriate flags
            if (META_FLAG_LC08_LANDSAT_METADATA_FILE in metadata.keys() and
                    META_FLAG_LC08_PRODUCT_CONTENTS in metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE].keys()):
                product_contents = metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE][META_FLAG_LC08_PRODUCT_CONTENTS]
                for __index__ in range(1, 12):
                    if META_FLAG_FILE_NAME_BAND_ + str(__index__) in product_contents.keys():
                        tiff_file_names.append(product_contents[META_FLAG_FILE_NAME_BAND_ + str(__index__)])
            # Check if the broken metadata contains the appropriate information
            elif META_FLAG_LC08_PRODUCT_CONTENTS in metadata.keys():
                product_contents = metadata[META_FLAG_LC08_PRODUCT_CONTENTS]
                for __index__ in range(1, 12):
                    if META_FLAG_FILE_NAME_BAND_ + str(__index__) in product_contents.keys():
                        tiff_file_names.append(product_contents[META_FLAG_FILE_NAME_BAND_ + str(__index__)])
            # Check if the broken metadata contains the appropriate information
            else:
                tiff_file_names = read_tiff_from_path()

            if tiff_file_names is []:
                tiff_file_names = read_tiff_from_path()

        return tiff_file_names

    # ****************************** #
    # ***** FUNCTIONAL METHODS ***** #
    # ****************************** #
    def _read_data(self, data_files: list):
        tmp_dataset = {}
        for __key__ in DICT_FLAG_LANDSAT_DATASET.keys():
            for __path__ in data_files:
                if __path__.name.__contains__(__key__):
                    print(f"Info:Read band <{__key__}>")
                    with rxr.open_rasterio(
                            str(__path__),
                            cache=False
                    ) as raster:
                        tmp_dataset[__key__] = raster.squeeze()
                        raster.close()
                    break
        self.data = tmp_dataset
        first_entry = self.data[list(self.data.keys())[0]]

        self.epsg_id = CRS(first_entry.rio.crs).to_epsg()
        self.epsg = f'EPSG:{self.epsg_id}'
        self.crs = CRS(self.epsg_id)

    def _readMetadata_TXT_File(self, in_file_path: Path):
        # Local function for recursing
        def _read_metadata_group(metadataFile, start):
            tmp_metadata = {}  # declare an empty temporary dictionary
            index = start  # set an index parameter equals to start
            # while index is less than the file_size (lines of the txt file)
            while index < metadataFile.__len__():
                # remove all spaces from the line and split on the first "=" character
                line_split = metadataFile[index].strip().replace(" = ", "=").replace(" =", "=").replace("= ", "=")
                line_split = line_split.split('=', 1)

                # If the line split is equal to 2 parts (GROUP, END_GROUP, or PARAMETER)
                if line_split.__len__() == 2:
                    command = line_split[0]  # Get the command/parameter_name
                    value = line_split[1]  # Get the value
                    # command == GROUP
                    if command == 'GROUP':
                        # Recurse for reading the parameter values of the new group
                        tmp_metadata[value], index = _read_metadata_group(metadataFile, index + 1)
                    # command == END_GROUP
                    elif command == 'END_GROUP':
                        # return the dictionary with the parameter values and the current index value
                        return tmp_metadata, index
                    # command is PARAMETER
                    else:
                        # Add the parameter to the dictionary
                        tmp_metadata[command] = str(value.replace('"', ''))
                # If line split is not equal to 2 (error case or END)
                else:
                    # return the dictionary with the parameter values and the current index value
                    return tmp_metadata, index
                index += 1  # increase the index
            # return the dictionary with the parameter values and the current index value (safety return)
            return tmp_metadata, index

        fp = Path(in_file_path)
        # set metadata to empty dict
        metadata = {}
        # Check if the file exists
        if os.path.exists(fp):
            # Read all lines in the txt file
            with open(fp) as __file__:
                metadata_file = __file__.readlines()
                __file__.close()
            # Read metadata
            metadata, _ = _read_metadata_group(metadata_file, 0)
        self.metadata = metadata

    def _read_metadata(self, metadata_files: list):
        # Check if the xml file exists in the directory
        xml_file = [x for x in metadata_files if str(x).__contains__(".xml")]
        if xml_file.__len__() == 0:  # if xml file does not exist
            # Then get the txt file and read it
            txt_file = [x for x in metadata_files if str(x).__contains__(".txt")][0]
            print(f"Info:Read metadata from <{str(txt_file)}>")
            self._readMetadata_TXT_File(in_file_path=Path(txt_file))
        else:  # Read the metadata from the xml file
            xml_file = str(xml_file[0])
            with open(xml_file, 'r') as o_xml_file:
                print(f"Info:Read metadata from <{xml_file}>")
                self._metadata = xmltodict.parse(o_xml_file.read())
                o_xml_file.close()

    def _get_meta_attrs(self):
        meta_attrs = {}
        if self.metadata.__len__() > 0:
            if META_FLAG_LC08_LANDSAT_METADATA_FILE in self.metadata.keys():
                meta_files = self.metadata[META_FLAG_LC08_LANDSAT_METADATA_FILE]
                for __key__ in meta_files.keys():
                    # PRODUCT CONTENTS
                    if __key__ == META_FLAG_LC08_PRODUCT_CONTENTS:
                        meta_entry = meta_files[META_FLAG_LC08_PRODUCT_CONTENTS]
                        # ORIGIN
                        if META_FLAG_ORIGIN in meta_entry.keys():
                            meta_attrs[META_FLAG_ORIGIN] = meta_entry[META_FLAG_ORIGIN]
                        # OBJECT IDENTIFIER
                        if META_FLAG_DIGITAL_OBJECT_IDENTIFIER in meta_entry.keys():
                            meta_attrs[META_FLAG_DIGITAL_OBJECT_IDENTIFIER] = meta_entry[
                                META_FLAG_DIGITAL_OBJECT_IDENTIFIER]
                        # LANDSAT PRODUCT ID
                        if META_FLAG_LANDSAT_PRODUCT_ID in meta_entry.keys():
                            meta_attrs[META_FLAG_LANDSAT_PRODUCT_ID] = meta_entry[META_FLAG_LANDSAT_PRODUCT_ID]
                        # PROCESSING LEVEL
                        if META_FLAG_PROCESSING_LEVEL in meta_entry.keys():
                            meta_attrs[META_FLAG_PROCESSING_LEVEL] = meta_entry[META_FLAG_PROCESSING_LEVEL]
                        # COLLECTION NUMBER
                        if META_FLAG_COLLECTION_NUMBER in meta_entry.keys():
                            meta_attrs[META_FLAG_COLLECTION_NUMBER] = meta_entry[META_FLAG_COLLECTION_NUMBER]
                        # COLLECTION CATEGORY
                        if META_FLAG_COLLECTION_CATEGORY in meta_entry.keys():
                            meta_attrs[META_FLAG_COLLECTION_CATEGORY] = meta_entry[META_FLAG_COLLECTION_CATEGORY]

                    # IMAGE_ATTRIBUTES
                    elif __key__ == META_FLAG_LC08_IMAGE_ATTRIBUTES:
                        meta_entry = meta_files[META_FLAG_LC08_IMAGE_ATTRIBUTES]
                        for __meta_key__ in meta_entry.keys():
                            meta_attrs[__meta_key__] = meta_entry[__meta_key__]

                    # LEVEL1_RADIOMETRIC_RESCALING
                    elif __key__ == META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING:
                        meta_entry = meta_files[META_FLAG_LC08_LEVEL1_RADIOMETRIC_RESCALING]
                        for __meta_key__ in meta_entry.keys():
                            meta_attrs[__meta_key__] = meta_entry[__meta_key__]

                    # LEVEL1_THERMAL_CONSTANTS
                    elif __key__ == META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS:
                        meta_entry = meta_files[META_FLAG_LC08_LEVEL1_THERMAL_CONSTANTS]
                        for __meta_key__ in meta_entry.keys():
                            meta_attrs[__meta_key__] = meta_entry[__meta_key__]

        return meta_attrs

    # ****************************** #
    # ***** OVERLOAD FUNCTIONS ***** #
    # ****************************** #
    def read(self, path: str):
        super().read(path)
        l8_files = list(self.p_path.iterdir())
        metadata_files = []
        data_files = []
        other_files = []
        for __file__ in l8_files:
            if __file__.name.split(str(self.p_path.name))[-1].__contains__('MTL'):
                metadata_files.append(__file__)
            elif __file__.name.split(str(self.p_path.name))[-1].__contains__('_B'):
                data_files.append(__file__)
            else:
                other_files.append(__file__)

        self._read_metadata(metadata_files)
        self._read_data(data_files)

    def reproject(self, epsg_id: str or int):
        super().reproject(epsg_id)

    def resample(self, spatial_resolution: float):
        super().resample(spatial_resolution)

    def clip(self, bounds: str or Path or gpd.GeoDataFrame):
        super().clip(bounds)

    def generate_product(self):
        super().generate_product()
        img_shape = (0, 0)
        index = 1
        for __key__ in self._data.keys():
            if self.product['coords'].__len__() == 0:
                self.product['coords'][ODC_DIM_KEY_BAND] = {'dims': (ODC_DIM_KEY_BAND,),
                                                           'attrs': {'axis': ODC_DIM_KEY_BAND,
                                                                     'long_name': 'spectral bands - radiometry',
                                                                     'standard_name': 'spectral_bands',
                                                                     'units': 'radiometry',
                                                                     },
                                                           'data': [f'B{str(index).zfill(2)}_{DICT_FLAG_LANDSAT_DATASET[__key__]} ']
                                                           }

                self.product['coords'][ODC_DIM_KEY_LATITUDE] = \
                    {'dims': (ODC_DIM_KEY_LATITUDE,),
                     'attrs': self.data[__key__].coords['y'].attrs,
                     'data': self.data[__key__].coords['y'].data,
                     'spatial_ref': self._data[__key__].coords['y'].spatial_ref.to_dict(),
                     'name': ODC_DIM_KEY_LATITUDE
                     }

                self.product['coords'][ODC_DIM_KEY_LONGITUDE] = \
                    {'dims': (ODC_DIM_KEY_LONGITUDE,),
                     'attrs': self.data[__key__].coords['x'].attrs,
                     'data': self.data[__key__].coords['x'].data,
                     'spatial_ref': self.data[__key__].coords['x'].spatial_ref.to_dict(),
                     'name': ODC_DIM_KEY_LONGITUDE
                     }
                size_x = self.product['coords'][ODC_DIM_KEY_LONGITUDE]['data'].shape[0]
                size_y = self.product['coords'][ODC_DIM_KEY_LATITUDE]['data'].shape[0]
                img_shape = (size_x, size_y)

            else:
                self.product['coords'][ODC_DIM_KEY_BAND]['data'].append(f'B{str(index).zfill(2)}_{DICT_FLAG_LANDSAT_DATASET[__key__]} ')
            index += 1

            tmp_data = self._data[__key__].data
            if img_shape != tmp_data.shape:
                tmp_data = Image.fromarray(tmp_data)
                tmp_data = tmp_data.resize(img_shape)
                tmp_data = np.array(tmp_data)

            self.product['data_vars'][self.p_path.name]['data'].append(tmp_data)
            self.product['attrs'] = self._get_meta_attrs()

    def exec_workflow(self, path: str or Path, epsg_id: str or int,
                      bounds: str or Path or gpd.GeoDataFrame, spatial_resolution: float,
                      b_clean_data: bool = True):
        super().exec_workflow(path, epsg_id, bounds, spatial_resolution, b_clean_data)

    def export_as_NetCDF(self, dir_path: str or Path or None = None):
        super().export_as_NetCDF(dir_path)


if __name__ == '__main__':
    test_path = '../../../../data/LC08_L1TP_183034_20240208_20240213_02_T1.tar'
    shp_path = '../../../../data/Piraeus/Piraeus_EPSG2100.shp'
    export_path = '../../../../data/export/'
    p = Landsat8()
    p.exec_workflow(test_path, 2100, shp_path, 15.0)
    p.export_as_NetCDF(export_path)
