from eo_products import *
from src.smart_datacubes.odc_framework.ODC_Flags import *
from eoreader.reader import Reader
from eoreader.bands import *


class Sentinel2(EarthObservationData):
    def __init__(self):
        super().__init__()
        self.band_dict = {
            CA: KEY_B1,
            BLUE: KEY_B2,
            GREEN: KEY_B3,
            RED: KEY_B4,
            VRE_1: KEY_B5,
            VRE_2: KEY_B6,
            VRE_3: KEY_B7,
            NIR: KEY_B8,
            NARROW_NIR: KEY_B8A,
            WV: KEY_B9,
            SWIR_CIRRUS: KEY_B10,
            SWIR_1: KEY_B11,
            SWIR_2: KEY_B12
        }

    # ******************************** #
    # ***** OVERLOADED FUNCTIONS ***** #
    # ******************************** #
    def read(self, path: str):
        super().read(path)
        if self.p_path.exists():
            tmp_data = {}
            s2_product = Reader().open(str(self.p_path), remove_tmp=True)
            set_epsg = True
            for __band__ in self.band_dict.keys():
                if __band__ in s2_product.get_existing_bands():
                    print(f"Info:Read band <{self.band_dict[__band__]}:{__band__}>")
                    raster = s2_product.load(__band__, pixel_size=10)
                    if set_epsg:
                        self.epsg_id = raster.rio.crs.to_epsg()
                        self.epsg = f'EPSG:{self.epsg_id}'
                        self.crs = CRS.from_epsg(self.epsg_id)
                    tmp_data[self.band_dict[__band__]] = raster.fillna(0).to_dataarray().squeeze()
                    tmp_data[self.band_dict[__band__]].close()
                    raster.close()
            self.data = tmp_data.copy()
            tmp_data.clear()
            s2_product.clean_tmp()
            s2_product.clear()

    def reproject(self, epsg_id: str or int):
        super().reproject(epsg_id)

    def resample(self, spatial_resolution: float):
        super().resample(spatial_resolution)

    def clip(self, bounds: str or Path or gpd.GeoDataFrame):
        super().clip(bounds)

    def generate_product(self):
        super().generate_product()
        img_shape = (0, 0)
        index = 1
        for __key__ in self._data.keys():
            if self.product['coords'].__len__() == 0:
                self.product['coords'][ODC_DIM_KEY_BAND] = {'dims': (ODC_DIM_KEY_BAND,),
                                                            'attrs': {'axis': ODC_DIM_KEY_BAND,
                                                                      'long_name': 'spectral bands - radiometry',
                                                                      'standard_name': 'spectral_bands',
                                                                      'units': 'radiometry',
                                                                      },
                                                            'data': [
                                                                f'B{str(index).zfill(2)}_{DICT_FLAG_SENTINEL_DATASET[__key__]} ']
                                                            }

                self.product['coords'][ODC_DIM_KEY_LATITUDE] = \
                    {'dims': (ODC_DIM_KEY_LATITUDE,),
                     'attrs': self.data[__key__].coords['y'].attrs,
                     'data': self.data[__key__].coords['y'].data,
                     'spatial_ref': self._data[__key__].coords['y'].spatial_ref.to_dict(),
                     'name': ODC_DIM_KEY_LATITUDE
                     }

                self.product['coords'][ODC_DIM_KEY_LONGITUDE] = \
                    {'dims': (ODC_DIM_KEY_LONGITUDE,),
                     'attrs': self.data[__key__].coords['x'].attrs,
                     'data': self.data[__key__].coords['x'].data,
                     'spatial_ref': self.data[__key__].coords['x'].spatial_ref.to_dict(),
                     'name': ODC_DIM_KEY_LONGITUDE
                     }
                size_x = self.product['coords'][ODC_DIM_KEY_LONGITUDE]['data'].shape[0]
                size_y = self.product['coords'][ODC_DIM_KEY_LATITUDE]['data'].shape[0]
                img_shape = (size_x, size_y)

            else:
                self.product['coords'][ODC_DIM_KEY_BAND]['data'].append(
                    f'B{str(index).zfill(2)}_{DICT_FLAG_SENTINEL_DATASET[__key__]} ')
            index += 1

            tmp_data = self._data[__key__].data
            if img_shape != tmp_data.shape:
                tmp_data = Image.fromarray(tmp_data)
                tmp_data = tmp_data.resize(img_shape)
                tmp_data = np.array(tmp_data)

            self.product['data_vars'][self.p_path.name]['data'].append(tmp_data)

    def exec_workflow(self, path: str or Path, epsg_id: str or int,
                      bounds: str or Path or gpd.GeoDataFrame, spatial_resolution: float,
                      b_clean_data: bool = True):
        super().exec_workflow(path, epsg_id, bounds, spatial_resolution, b_clean_data)

    def export_as_NetCDF(self, dir_path: str or Path or None = None):
        super().export_as_NetCDF(dir_path)

    # ***************************** #
    # ***** PRIVATE FUNCTIONS ***** #
    # ***************************** #

    # def _importDataFromDir(self, in_dir_path: str, pixel_size=10):
    #     super()._importDataFromDir(in_dir_path)


if __name__ == '__main__':
    test_path = '../../../../data/S2B_MSIL2A_20230831T091559_N0509_R093_T34TFK_20230831T121425.SAFE.zip'
    shp_path = '../../../../data/Thermaikos/Thermaikos.shp'
    export_path = '../../../../data/export/'
    p = Sentinel2()
    p.exec_workflow(test_path, None, shp_path, 10.0)
    p.export_as_NetCDF(export_path)
