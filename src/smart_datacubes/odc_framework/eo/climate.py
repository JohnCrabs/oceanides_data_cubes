from eo_products import *
import xarray as xr


class ClimateData(EarthObservationData):
    def __init__(self):
        super().__init__()

    # ******************************** #
    # ***** OVERLOADED FUNCTIONS ***** #
    # ******************************** #
    def read(self, path: str):
        super().read(path)
        if self.p_path.exists():
            climate_ds = xr.open_dataset(str(self.p_path))
            if climate_ds.rio.crs is None:
                climate_ds = climate_ds.rio.write_crs('3035')
            self.data = {'Climate': climate_ds}

    def reproject(self, epsg_id: str or int):
        super().reproject(epsg_id)

    def resample(self, spatial_resolution: float):
        super().resample(spatial_resolution)

    def clip(self, bounds: str or Path or gpd.GeoDataFrame):
        super().clip(bounds)

    # TODO - Write this function
    def generate_product(self):
        super().generate_product()
        self.product = self.data['Climate'].to_dict()

    def exec_workflow(self, path: str or Path, epsg_id: str or int = None,
                      bounds: str or Path or gpd.GeoDataFrame = None, spatial_resolution: float = None,
                      b_clean_data: bool = True):
        # super().exec_workflow(path, epsg_id, bounds, spatial_resolution, b_clean_data)
        self.read(path)
        self.generate_product()

    def export_as_NetCDF(self, dir_path: str or Path or None = None):
        super().export_as_NetCDF(dir_path)

    # ***************************** #
    # ***** PRIVATE FUNCTIONS ***** #
    # ***************************** #

    # def _importDataFromDir(self, in_dir_path: str, pixel_size=10):
    #     super()._importDataFromDir(in_dir_path)


if __name__ == '__main__':
    test_path = '../../../../data/climate_data/total_precipitation_2019_2021.grib'
    shp_path = '../../../../data/Piraeus/Piraeus_EPSG2100.shp'
    export_path = '../../../../data/export/'
    p = ClimateData()
    p.exec_workflow(test_path, 2100, shp_path, 10.0)
    p.export_as_NetCDF(export_path)
