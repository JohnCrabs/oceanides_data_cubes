import shutil
import xarray as xr
import xtarfile as tarfile

from pyproj import CRS
from pathlib import Path
from zipfile import ZipFile


class FrameworkODC:
    def __init__(self):
        self._product: dict = {}
        self._epsg: str = "EPSG:4326"
        self._epsg_id: str or int = 4326
        self._crs: CRS = CRS.from_epsg(4326)

        self._p_path: Path = Path("")
        self._b_del_p_path: bool = False

    # ***************************** #
    # ***** GETTERS / SETTERS ***** #
    # ***************************** #

    @property
    def product(self):
        return self._product

    @product.setter
    def product(self, new_product: dict):
        if type(new_product) is dict:
            self._product = new_product

    @property
    def epsg(self):
        return self._epsg

    @epsg.setter
    def epsg(self, new_epsg: str):
        if new_epsg != self.epsg:
            self._epsg = new_epsg

    @property
    def epsg_id(self):
        return self._epsg_id

    @epsg_id.setter
    def epsg_id(self, new_epsg_id: str or int):
        if new_epsg_id != self.epsg_id:
            self._epsg_id = new_epsg_id

    @property
    def crs(self):
        return self._crs

    @crs.setter
    def crs(self, new_crs: CRS):
        if new_crs != self.crs:
            self._crs = new_crs

    @property
    def p_path(self):
        return self._p_path

    @p_path.setter
    def p_path(self, new_p_path: str or Path):
        if new_p_path != self.p_path:
            self._p_path = Path(new_p_path)

    @property
    def b_del_p_path(self):
        return self._p_path

    @b_del_p_path.setter
    def b_del_p_path(self, state: bool):
        if state != self.b_del_p_path:
            self._b_del_p_path = state

    # **************************** #
    # ***** WORKFLOW METHODS ***** #
    # **************************** #
    def extract(self, path: str or Path):
        fp = Path(path)
        if fp.exists():
            # Check if file is in ZIP format
            if fp.name.__contains__(".zip"):
                print(f"Info:Extracting file <{fp.name}>")
                self.p_path = str(fp).split(".zip")[0]
                self.b_del_p_path = True
                with ZipFile(str(fp)) as ZipObject:
                    ZipObject.extractall(self.p_path.parent)
            # Check if file is in TAR format
            elif fp.name.__contains__(".tar"):
                print(f"Info:Extracting file <{fp.name}>")
                self.p_path = str(fp).split(".tar")[0]
                self.b_del_p_path = True
                with tarfile.open(str(fp), 'r') as TarObject:
                    TarObject.extractall(self.p_path)
                    TarObject.close()
            # Check if file is Directory
            elif fp.is_dir():
                self.p_path = fp
                self.b_del_p_path = False
            else:
                self.p_path = fp
                self.b_del_p_path = False

    def read(self, path: str or Path):
        self.extract(path)

    def reproject(self, epsg_id: str or int or None):
        pass

    def clip(self, bounds: str or Path or gpd.GeoDataFrame):
        pass

    def resample(self, spatial_resolution: float):
        pass

    def generate_product(self):
        pass

    def clean(self):
        if self.b_del_p_path:
            print(f"Info:Remove data in path <{self.p_path}>")
            shutil.rmtree(self.p_path, ignore_errors=True, onerror=None)

    def exec_workflow(self, path: str or Path, epsg_id: str or int,
                      bounds: str or Path or gpd.GeoDataFrame, spatial_resolution: float,
                      b_clean_data: bool = True):
        self.read(path)
        self.reproject(epsg_id)
        self.clip(bounds)
        self.resample(spatial_resolution)
        self.generate_product()
        if b_clean_data:
            self.clean()

    def export_as_NetCDF(self, dir_path: str or Path or None = None):
        if dir_path is None:
            dir_path = self.p_path.parent
        fp = Path(str(dir_path) + f'/{self.p_path.name}.nc')

        product_netCDF = xr.Dataset.from_dict(self.product)
        print(f"Info:Save product as NetCDF file in path <{str(fp)}>")
        product_netCDF.to_netcdf(str(fp))


if __name__ == '__main__':
    print("Info: Hello World!")
